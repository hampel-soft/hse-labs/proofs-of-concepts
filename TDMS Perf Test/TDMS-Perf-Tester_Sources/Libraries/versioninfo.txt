====================================================
hampelsoft-libraries.lvproj
====================================================

HAMPEL SOFTWARE ENGINEERING
Create Better NI LabVIEW Software!
www.hampel-soft.com


----------------------------------------------------
Git Tag No: v0.14.0
Git Commit: 908614d21b750168f50d83a77ff8c4b0f6b747c5
----------------------------------------------------
Build Spec: hselib_source_dist
Build Trgt: My Computer
Build Time: 2018-10-14 18:25
----------------------------------------------------
LabVIEW:    2016 (16.0f5)
OpSystem:   Windows 7 Home Premium Service Pack 1
OS Build:   7601
----------------------------------------------------


====================================================
Changelog for this release
====================================================

2018-10-14 18:23:30 +0200
Merge branch 'release/v0.14.0'


2018-10-14 18:22:34 +0200
Merge branch 'feature/restructure-for-demoapp' into develop


2018-10-14 18:22:24 +0200
added helper VI for requesting module UI display

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Built automatically with HSE cmdline-tools (v1.6.0)
Visit www.hampel-soft.com to learn more about CI/CD
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~